#ifndef RFIDReader_H
#define RFIDReader_H

#include <Arduino.h>
#include <SPI.h>
#include <MFRC522.h>

class RFIDReader{
private:
	MFRC522 *mfrc522;
	unsigned char uid[4];
	char uidHex[9];

public:
	RFIDReader(MFRC522 &mfrc522);

	void init(void);
	bool read(void);
	const char *getUidHex(void) { return uidHex; };
};

#endif

#include "RFIDReader.h"

RFIDReader::RFIDReader(MFRC522 &mfrc522) {
	this->mfrc522 = &mfrc522;
}

void RFIDReader::init(void) {
	SPI.begin();
	mfrc522->PCD_Init();
}

bool RFIDReader::read(void){
	if (!mfrc522->PICC_IsNewCardPresent()) return false;
	if (!mfrc522->PICC_ReadCardSerial()) return false;
	MFRC522::PICC_Type piccType = mfrc522->PICC_GetType(mfrc522->uid.sak);
	if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
		  piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
		  piccType != MFRC522::PICC_TYPE_MIFARE_4K) return false;
	for (byte i = 0; i < 4; i++) {
		uid[i] = mfrc522->uid.uidByte[i];
	}
	mfrc522->PICC_HaltA();
	mfrc522->PCD_StopCrypto1();
	sprintf(uidHex,"%02X%02X%02X%02X", uid[0], uid[1], uid[2], uid[3]);
	return true;
}
